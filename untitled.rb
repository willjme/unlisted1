# Links contained inside a tweet
class TweetUrl
  # Short URL. Shortened by twitter's URL shortener. Format is https://t.co/...
  attr_accessor :url
  # How the URL is displayed in the tweet
  attr_accessor :display_url
  # Human-readable summary of the link.
  # If you pass #use_mechanize:true to TweetParser.get_tweets, the URL is scraped for the HTML page title
  attr_accessor :page_title
  # Full, original, URL
  attr_accessor :expanded_url
end